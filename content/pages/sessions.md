+++
title = "Sessions"
path = "sessions"
+++

# Rust Belt Rust Conference Sessions

## Day one - workshops

<table class="schedule">
    <tr>
        <th>Morning: 9am-12pm</th>
        <th>Afternoon: 2pm-5pm</th>
    </tr>
    <tr>
        <td colspan="2"><a href="#rustbridge">Rustbridge</a></td>
    </tr>
    <tr>
        <td colspan="2"><a href="#shell">Build your own shell in Rust!</a></td>
    </tr>
    <tr>
        <td><a href="#intro">Intro to Rust</a></td>
        <td><a href="#escher">Escher in Rust</a></td>
    </tr>
    <tr>
        <td><a href="#debugging">Where We're Going, We Don't Need Println!</a></td>
        <td><a href="#gauges">Gauges and Logs: Instrumenting Rust</a></td>
    </tr>
    <tr>
        <td colspan="2">Unconference/hallway track</td>
    </tr>
</table>

## Day two - single track

<table class="schedule">
    <tr>
        <th>Time</th>
        <th>Talk</th>
    </tr>
    <tr>
        <td>9:00 am</td>
        <td>Announcements/coffee</td>
    </tr>
    <tr>
        <td>9:30 am</td>
        <td><a href="#core">Core team talk</a></td>
    </tr>
    <tr>
        <td>10:00 am</td>
        <td><a href="#syntax">Syntax conveniences afforded by the compiler</a></td>
    </tr>
    <tr>
        <td>10:30 am</td>
        <td>BREAK</td>
    </tr>
    <tr>
        <td>11:00 am</td>
        <td><a href="#monotron">Monotron - a 1980s style home computer written in Rust</a></td>
    </tr>
    <tr>
        <td>11:30 am</td>
        <td><a href="#actix">Actix and Actors in Rust</a></td>
    </tr>
    <tr>
        <td>12:00 pm</td>
        <td>LUNCH</td>
    </tr>
    <tr>
        <td>2:00 pm</td>
        <td>Lightning Talks</td>
    </tr>
    <tr>
        <td>2:30 pm</td>
        <td><a href="#percy">Percy: Isomorphic Web Apps with Rust + WebAssembly</a></td>
    </tr>
    <tr>
        <td>3:00 pm</td>
        <td>BREAK</td>
    </tr>
    <tr>
        <td>3:30 pm</td>
        <td><a href="#evolving">Evolving API design in Rust</a></td>
    </tr>
    <tr>
        <td>4:00 pm</td>
        <td><a href="#community">Maintaining the Community</a></td>
    </tr>
    <tr>
        <td>4:30 pm</td>
        <td>BREAK</td>
    </tr>
    <tr>
        <td>5:00 pm</td>
        <td><a href="#fast">Move fast and don't break things: High-performance networking in Rust</a></td>
    </tr>
    <tr>
        <td>5:30 pm</td>
        <td><a href="#crates">State of the crates.io</a></td>
    </tr>
</table>

{{ talk(id="shell") }}
{{ talk(id="escher") }}
{{ talk(id="gauges") }}
{{ talk(id="evolving") }}
{{ talk(id="actix") }}
{{ talk(id="monotron") }}
{{ talk(id="percy") }}
{{ talk(id="syntax") }}
{{ talk(id="fast") }}
{{ talk(id="rustbridge") }}
{{ talk(id="intro") }}
{{ talk(id="debugging") }}
{{ talk(id="core") }}
{{ talk(id="community") }}
{{ talk(id="crates") }}

<div style="display: none">
  <svg>
    <symbol id="twitter" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
      <path d="M16 3.038c-.59.26-1.22.437-1.885.517.677-.407 1.198-1.05 1.443-1.816-.634.37-1.337.64-2.085.79-.598-.64-1.45-1.04-2.396-1.04-1.812 0-3.282 1.47-3.282 3.28 0 .26.03.51.085.75-2.728-.13-5.147-1.44-6.766-3.42C.83 2.58.67 3.14.67 3.75c0 1.14.58 2.143 1.46 2.732-.538-.017-1.045-.165-1.487-.41v.04c0 1.59 1.13 2.918 2.633 3.22-.276.074-.566.114-.865.114-.21 0-.41-.02-.61-.058.42 1.304 1.63 2.253 3.07 2.28-1.12.88-2.54 1.404-4.07 1.404-.26 0-.52-.015-.78-.045 1.46.93 3.18 1.474 5.04 1.474 6.04 0 9.34-5 9.34-9.33 0-.14 0-.28-.01-.42.64-.46 1.2-1.04 1.64-1.7z" fill-rule="nonzero"/>
    </symbol>
    <symbol id="globe" viewbox="0 0 420 420">
      <path stroke="#828282" fill="none" stroke-width="20" d="M209,15a195,195 0 1,0 2,0zm1,0v390m195-195H15M59,90a260,260 0 0,0 302,0 m0,240 a260,260 0 0,0-302,0M195,20a250,250 0 0,0 0,382 m30,0 a250,250 0 0,0 0-382"/>
    </symbol>
  </svg>
</div>
