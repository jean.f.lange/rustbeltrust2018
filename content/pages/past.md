+++
title = "Prior years"
path = "past"
+++
# Rust Belt Rust Conference prior years

## Rust Belt Rust 2017

Last year's Rust Belt Rust Conference was held in Columbus, OH.

- [session descriptions](http://conf2017.rust-belt-rust.com/sessions.html)
- [videos of the talks](https://www.youtube.com/playlist?list=PLgC1L0fKd7Ul71lD_cImGuMxsZ6J8fa06)
- [2017 web site](http://conf2017.rust-belt-rust.com/)

## Rust Belt Rust 2016

The first Rust Belt Rust Conference was held in Pittsburgh, PA.

- [session descriptions](http://conf2016.rust-belt-rust.com/sessions/)
- [videos of the talks](https://www.youtube.com/playlist?list=PLgC1L0fKd7UmdG82JOEE0uzXci1XY61xU)
- [2016 web site](http://conf2016.rust-belt-rust.com/)
